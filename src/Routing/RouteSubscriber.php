<?php

namespace Drupal\routing_alter\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Component\Discovery\YamlDiscovery;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {
  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    $module_list = $this->moduleHandler->getModuleList();
    $routes_from_modules = $this->getRouteAlterDefinitions();
    $routes_from_modules = array_intersect_key($routes_from_modules, $module_list);
    foreach ($routes_from_modules as $routes) {
      foreach ($routes as $name => $info) {
        /* @var \Symfony\Component\Routing\Route $route */
        $route = $collection->get($name);
        if (!empty($route)) {
          if (isset($info['path'])) {
            $route->setPath($info['path']);
          }
          if (isset($info['defaults'])) {
            $route->setDefaults(array_merge($route->getDefaults(), $info['defaults']));
          }
          if (isset($info['requirements'])) {
            $route->setRequirements(array_merge($route->getRequirements(), $info['requirements']));
          }
          if (isset($info['options'])) {
            $route->setOptions(array_merge($route->getOptions(), $info['options']));
          }
        }
        else {
          new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }
      }
    }
  }

  /**
   * Returns an array with all the *.routing.alter.yml files found.
   *
   * @return array
   */
  protected function getRouteAlterDefinitions() {
    if (!isset($this->yamlDiscovery)) {
      $this->yamlDiscovery = new YamlDiscovery('routing.alter', $this->moduleHandler->getModuleDirectories());
    }
    return $this->yamlDiscovery->findAll();
  }
}