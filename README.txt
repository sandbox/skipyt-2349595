README.txt
==========

Allows developers to alter routes from yaml files.

To alter an existing route a new yaml file should be created in the module's
directory.

EXAMPLE
=======

If you want to alter the route for the node add page, to change the page title and the permission.

1. Create a new file mymodule.routing.alter.yml
2. Add the modified properties in the route.
  node.add_page:
    defaults:
      _title: 'Add content - title modified'
    requirements:
      _permission: 'content contributor'


The other properties of the route will remain untouched.
IF two modules are altering the same route, they will be applied regarding to the module's weight.

AUTHOR/MAINTAINER
======================
- Moshe Weitzman <weitzman at tejasa DOT com> http://www.acquia.com
- Hans Salvisberg <drupal at salvisberg DOT com>
- Pedro Cambra <https://drupal.org/user/122101/contact> http://www.ymbra.com/
